#!/usr/bin/env bash
version=0.0.1
# functions --------------------------------
is_number() [[ $* == +([0-9]) ]]

validate_fix() {
  is_number "$*" \
    || { echo "${err_message[1]}"; return 1; }
}

cawk() {
  [[ $* ]] \
    && awk "BEGIN {printf \"%.${fix}f\n\", ($*)}" \
    || echo "${err_message[0]}"
}

repl() {
  read -ep ': '
  case $REPLY in
    f*) validate_fix "${REPLY##f*( )}" \
          && fix=${REPLY##f*( )}
      ;;
    help*) echo "$usage";;
    license*) echo "$(< LICENSE)";;
    quit*) exit;;
    *)  cawk "$REPLY"
  esac
}

inline() {
  local opt OPTIND OPTARG
  while getopts ':f:hv' opt; do
    case $opt in
      f)  validate_fix "$OPTARG" \
        && fix=$OPTARG \
        || exit 1;;
      h)  echo "$usage"; exit;;
      v)  echo "v$version"; exit;;
    esac
  done
  shift $((OPTIND - 1))
  [[ -s /dev/stdin ]] \
    && expr=$(< /dev/stdin) \
    || expr=$*
  cawk "$expr"
}

main() {
  if (($#)); then
    inline "$@"
  else
    printf '%s\n' "${prompt_repl[@]}"
    while :; do repl; done
  fi
}
# messages ---------------------------------
usage='  Usage: ./cawk.sh [-f PRECISION] [EXPRESSION]

DESCRIPTION
  Calculator REPL

OPTIONS
  General options
    -f      Precision decimal
'
err_message=(
  [0]='ERROR: Missing expression.'
  [1]='ERROR: Fix must be a integer.'
)

prompt_repl=(
  [0]="Cawk $version"
  [1]='Type "help", "license" for more information or CTRL+C to quit'
)
# main -------------------------------------
shopt -s extglob
fix=0
main "$@"
